var express = require('express'),
	fs = require('fs'),
	bodyParser = require('body-parser'),
	port = 8080,
	chartRouter = require("./api/getChartData.js"),
	app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/dist'));
app.use(function(req, res, next) {
  	res.setHeader('Access-Control-Allow-Origin', '*');
  	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
  	next();
});
app.get("/getData",chartRouter);
app.listen(port);