var express = require('express'),
	fs = require('fs'),
	router = express.Router();

router.route("/getData").get(function(req,res){
	fs.readFile("./data/series.csv","utf8",function(err,data){
		if(err){
			res.send(err);
		}else{
			/*console.log(data);*/
			res.send(data);
		}
	})
})

module.exports = router;