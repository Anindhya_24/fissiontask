"use strict";
var gulp = require("gulp"),
	concat = require("gulp-concat"),
	copy = require("gulp-copy"),
	express = require("express"),
	bodyParser = require('body-parser'),
	exec = require('child_process').exec;

var config = {
	copy : {
		source : [ "src/*.html"],
		destination : "dist/"
	},
	css : {
		source : ["src/css/**/*.css"],
		destination : "dist/css/",
		dest_file : "app.min.css"
	},
	js : {
		source : [	"./node_modules/angular/angular.js",
					"./node_modules/angular-ui-router/release/angular-ui-router.js",
					"./node_modules/highcharts/highcharts.js",
					"src/appConfig.js",
					"src/js/**/*.js"],
		destination : "dist/js/",
		dest_file : "app.min.js"
	}
};

function startServer(){
	exec('node server.js', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
    });
}

gulp.task("concat_css", function() {
	return gulp.src( config.css.source )
    	.pipe(concat( config.css.dest_file, {newLine: ';'} ))
    	.pipe(gulp.dest( config.css.destination))
})

gulp.task("concat_js", function() {
	return gulp.src( config.js.source )
    	.pipe(concat( config.js.dest_file))
    	.pipe(gulp.dest( config.js.destination))
})

gulp.task("copy", function(){
	return gulp.src(config.copy.source)
    .pipe(copy(config.copy.destination, { prefix: 1 }) )
    //.dest(config.copy.destination);
})

gulp.task('build', ['copy','concat_css','concat_js'], function() {
     
});

gulp.task("watchFiles", function(){
	var source = config.copy.source.concat( config.css.source, config.js.source );
	gulp.watch(source, ['build']);
})

gulp.task('serve', ['build', 'watchFiles'], function() {
    //call the server
    startServer();
});
gulp.task('default', ['serve']);