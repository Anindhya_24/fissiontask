var app = angular.module('fissionTaskApp',['ui.router']);
app.config(function($stateProvider,$urlRouterProvider,$locationProvider){
	$locationProvider.html5Mode(false);
	$locationProvider.hashPrefix('');

	$urlRouterProvider.otherwise('/main');
	var main ={
		name: 'main',
		url:'/main',
		template:'<div ui-view></div>',
		controller:function($scope,$state){
			$state.go("main.upload")
		}
	};
	var upload ={
		name: 'main.upload',
		url:'/upload',
		templateUrl:'UploadData.html',
		controller:'uploadController'
	};
	var graph = {
		name:'main.graph',
		url:'/graph',
		params:{
			key:null
		},
		templateUrl:'graph.html',
		controller:'graphController'
	}
	$stateProvider
	.state(upload)
	.state(main)
	.state(graph)
})