var app = angular.module('fissionTaskApp');
app.controller('uploadController',function($scope,$http,$state,dataService){
	$scope.uploadCSV = function(){
		dataService.loadData().then(function(data){
			$scope.data = dataService.parseCSV(data.data);
			dataService.setGraphData($scope.data);
			//console.log($scope.data);
		})
	}
	$scope.getGraph = function(series){
		$state.go("main.graph",{key:series});
	}
})
