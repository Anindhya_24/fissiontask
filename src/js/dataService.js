var app = angular.module('fissionTaskApp');
app.factory('dataService',function($http){
	var _graphData = null;
	return{
		loadData: function(){
			var promise = $http.get("/getData").then(dataSuccess,dataError);
			function dataSuccess(data){
				console.log(data.data);
				return data;
			}
			function dataError(error){
				console.log(error);
			}
			return promise;
		},
		parseCSV:function(data){
			var line,seriesData = [],tempArr = [],lineData={};
			line = data.split('\n');
			for (var i = 0; i < line.length; i++) {
				tempArr = line[i].split(',');
				lineData[tempArr[0]] = [];
				for (var j = 1; j < tempArr.length; j++){
					lineData[tempArr[0]].push({
						year:tempArr[j].split('|')[0],
						count:tempArr[j].split('|')[1]
					})
				}
			}
			return lineData;
		},
		setGraphData: function(graphData){
			_graphData = graphData;
		},
		getGraphData:function(){
			if(!angular.isDefined(_graphData)){
	            return null
	         }
	         return _graphData;
		}
	}
})