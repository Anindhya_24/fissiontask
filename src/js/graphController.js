var app = angular.module('fissionTaskApp');
app.controller('graphController',function($scope,$stateParams,$filter,dataService){
	var gData = dataService.getGraphData(),
		series = $stateParams.key,
	    categories = [],
	    categoriesValue =[];
    
    for(var i=0; i<gData[series].length;i++){
    	categories.push(gData[series][i].year);
    	categoriesValue.push(parseInt(gData[series][i].count));
    }

	Highcharts.chart('container', {
	    xAxis: {
	        categories: categories
	    },
	    title: {
	        text: series +' count per year'
	    },
	    tooltip:{
	    	useHTML: true,
	    	formatter:function(){
	    		return 'The value for <b>' + this.x + '</b><br/> is <b>' + this.y + '</b>, in '+ series;
	    	}
	    },
	    yAxis: { 
	    	title: { text: 'count' },
	    	labels: {
	                    useHTML: true,
	                    style: {
	                        color: "#96be8a"
	                    }
	                }
	     },
	    series: [{
	    	showInLegend: false,
	        data: categoriesValue
	    }]
	});
})